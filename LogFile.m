classdef LogFile < handle
    %LOGFILE Used to create a log file
    %   Call LogFile([], './path/to/log/file.csv') to create a logfile
    %   use AppendVector & AppendMatrix to write to the file
    
    properties
        fileName; 
        fid; 
    end
    
    methods
        %% LogFile
        % Create a logfile
        function self = LogFile(self, filename)
            %LOGFILE Construct an instance of this class
            %   Detailed explanation goes here
            self.fileName = filename;
            
            self.fid = fopen(self.fileName, 'a+');
            
        end
        
        %% AppendVector
        % Append a vector to the file
        function AppendVector(self, data)
            %Append Append data to the file specified in the constructor
            %datestr(datetime,'HH:MM:SS.FFF')
            for i = 1:numel(data)
                fwrite(self.fid, string(data(i)) + "," );
            end
            fwrite(self.fid, newline); 
        end
        
        %% AppendMatrix
        % Append a matrix to the logFile
        function AppendMatrix(self, data)
            for i = 1:numel(data(:,1))
                %Log rows
                for j = 1:numel(data(1,:))
                    fwrite(self.fid, string(data(i,j)) + "," );
                end
                fwrite(self.fid, newline);
            end
        end
        
        %% AppendString
        % Append a string to the file 
        function AppendString(self, data)
            fwrite(self.fid, string(data) + newline);
        end
    end
end

