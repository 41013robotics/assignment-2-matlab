classdef ArduinoClient
    %ARDUINOCLIENT Code to interface with custom arduino code
    %   This code interacts with an arduino, which has been programmed with
    %   the code in the folder 'arduino_server' 
    
    properties
        arduinoSerialPort;              % Serial port we would like to connect to 
        ultrasonicCmd = 'ultrasonic';   % Definition of a command
    end
    
    methods
        function self = ArduinoClient(serialPort)
            %ARDUINOCLIENT Construct an instance of this class
            %   First all serial ports will be closed. Then the specified
            %   port will be opened at 115200 baud
            try
            fclose(instrfind);              % Close all open serial ports 
            catch
                disp('No serial ports found')
            end
            self.arduinoSerialPort = serial(serialPort, 'BaudRate', 115200);
            fopen(self.arduinoSerialPort);             
        end
        
        function reading = getUltrasonicVal(self)
            %GETULTRASONICVAL Returns the distance detected by the
            %ultrasonic (double)             
            fprintf(self.arduinoSerialPort, self.ultrasonicCmd);
            reading = str2double(fgetl(self.arduinoSerialPort)); 
        end
        function close(self)
            %CLOSE Elegantly disconnects from arduino
            fclose(self.arduinoSerialPort);  
        end
    end
end

