function varargout = GUI(varargin)
% GUI MATLAB code for GUI.fig
%      GUI, by itself, creates a new GUI or raises the existing
%      singleton*.
%
%      H = GUI returns the handle to a new GUI or the handle to
%      the existing singleton*.
%
%      GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GUI.M with the given input arguments.
%
%      GUI('Property','Value',...) creates a new GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before GUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to GUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help GUI

% Last Modified by GUIDE v2.5 04-Jun-2018 21:13:35

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @GUI_OpeningFcn, ...
    'gui_OutputFcn',  @GUI_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end


if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end

% End initialization code - DO NOT EDIT

% --- Executes just before GUI is made visible.
function GUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to GUI (see VARARGIN)

% Choose default command line output for GUI
handles.output = hObject;

% Create Hand robot
handles.robotHand = Hand();
handles.robotHand.GetHandRobot();
handles.robotHand.model.base = transl(-1.2,0,0.1);
handles.robotHand.PlotAndColourRobot(); %plot the robot in the GUI
hold on;

% Create Cyton robot
handles.cytonRobotOne = CYTON();
handles.cytonRobotOne.PlotAndColourRobot();

% Create & display light curtain
steps = 2;
qR1 = [0,0,0];
qR2 = [pi,0,0];
qMatrix = jtraj(qR1,qR2,steps);

maxReach = 0.55;
centerpnt = [handles.cytonRobotOne.model.base(1,4), handles.cytonRobotOne.model.base(2,4), handles.cytonRobotOne.model.base(3,4)];
plotOptions.plotEdges = true;
[handles.vertex, handles.faces,handles.faceNormals] = RectangularPrism(maxReach *2, centerpnt,plotOptions);
CollisionLightCurtain(handles.robotHand.model,handles.robotHand.model.getpos(),handles.vertex,handles.faces,handles.faceNormals)

% Setup arduino - Note; this will close all opened serial ports 
handles.arduino = ArduinoClient("/dev/ttyACM0");

% Setup timer to regularly check for collisions
handles.checkCollisionTimer = timer('TimerFcn', {@checkForCollions, hObject}, 'ExecutionMode', 'fixedRate', 'Period', 0.2, 'StartDelay', 5);

% Create coffee pods
handles.bluePod = MeshObject([],'3D Models/pods/podBlue.ply');
handles.bluePod.SetPos(transl(0.1822, .05709 + 0.08, 0.005 +  handles.bluePod.zMax/2));
handles.BCpPanelPosx.String = string(0.1822);
handles.BCpPanelPosy.String = string(round(.05709 + 0.08,4));
handles.BCpPanelPosz.String = string(0.005);

handles.greenPod = MeshObject([],'3D Models/pods/podGreen.ply');
handles.greenPod.SetPos(transl(0.1822, .05709, 0.005 +  handles.greenPod.zMax/2));
handles.GCpPanelPosx.String = string(0.1822);
handles.GCpPanelPosy.String = string(.05709);
handles.GCpPanelPosz.String = string(0.005);

handles.redPod = MeshObject([],'3D Models/pods/podRED.ply');
handles.redPod.SetPos(transl(0.1822, -(.08 - .05709), 0.005 + handles.redPod.zMax/2));
handles.RCpPanelPosx.String = string(0.1822);
handles.RCpPanelPosy.String = string(round(-(.08 - .05709),4));
handles.RCpPanelPosz.String = string(0.005);

% Create E-stop button 
handles.eStop = MeshObject([], '3D Models/e-stop/E-stop.ply');
handles.eStop.SetPos(transl(0, 0.7, handles.eStop.zMax/2));

% Setup timer to continually update robot from HW. NOTE: This is not
% currently implemented due to limitations in the robotics toolbox
handles.posUpdateTimer = timer('TimerFcn', {@updateSimFromHw, hObject}, 'ExecutionMode', 'fixedRate', 'Period', 0.2, 'StartDelay', 5);

% Update handles structure
guidata(hObject, handles);
start(handles.checkCollisionTimer);

% UIWAIT makes GUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);

% --- Outputs from this function are returned to the command line.
function varargout = GUI_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

%% ===================================================================== %%
%%                  checkForCollions                                     %%
%  This code is called regularly by a timer. It's period is currently 0.2 % 
%   seconds. It checks for collisions with the light curtain & checks the % 
%   value received by the arduino                                         %
%% ===================================================================== %%
function checkForCollions(onj, event, hFigure)
handles = guidata(hFigure);
% Check for collision with hand
if (CollisionLightCurtain(handles.robotHand.model,[0],handles.vertex,handles.faces,handles.faceNormals) == 1)
    handles.handPositionPanelCollision.String = "LIGHT CURTAIN TRIGGERED";
    handles.handPositionPanelCollision.ForegroundColor = [1 0 0];
    disp("COLLISION DETECTED: Light Curtain");
    disp("When the area is safe the robot will start again. Please remove your hand from the light curtain.")
    pause(3)
elseif (handles.arduino.getUltrasonicVal() < 50)
    handles.handPositionPanelCollision.String = "PROXIMITY SENSOR TRIGGERED";
    handles.handPositionPanelCollision.ForegroundColor = [1 0 0];
    disp("COLLISION DETECTED: Arduino");
    disp("When the area is safe the robot will start again. Please remove your hand from the light curtain.")
    pause(3)
else
    handles.handPositionPanelCollision.String = "No Collision";
    handles.handPositionPanelCollision.ForegroundColor = [.4706 0.6706 0.1882];
end

%% ===================================================================== %%
%%                  updateSimFromHw                                      %%
%  This code updates the simulation robot from the real robots            %
%   position                                                              %
%% ===================================================================== %%
function updateSimFromHw(obj, event, hFigure)

handles = guidata(hFigure);
%updateRobotFromHw(handles);
handles.cytonRobotOne.UpdateSimFromHw();

%% ===================================================================== %%
%                       Teach orientation callbacks
%% ===================================================================== %%
% orientationPanelRoll_Callback
%    Callback fired when the roll value in changed on the orientation panel
function orientationPanelRoll_Callback(hObject, eventdata, handles)
moveRobotToPose(handles);

% orientationPanelPitch_Callback
%    Callback fired when the pitch value in changed on the orientation panel
function orientationPanelPitch_Callback(hObject, eventdata, handles)
moveRobotToPose(handles);

% orientationPanelYaw_Callback
%    Callback fired when the yaw value in changed on the orientation panel
function orientationPanelYaw_Callback(hObject, eventdata, handles)
moveRobotToPose(handles);

%% ===================================================================== %%
%                     Teach Position callbacks
%% ===================================================================== %%
% positionPanelX_Callback
%    Callback fired when the x position value in changed on the position panel
function positionPanelX_Callback(hObject, eventdata, handles)
moveRobotToPosition(handles);

% positionPanelY_Callback
%    Callback fired when the y position value in changed on the position panel
function positionPanelY_Callback(hObject, eventdata, handles)
moveRobotToPosition(handles);

% positionPanelZ_Callback
%    Callback fired when the z position value in changed on the position panel
function positionPanelZ_Callback(hObject, eventdata, handles)
moveRobotToPosition(handles);

%% ===================================================================== %%
%                     Teach joint angle callbacks
%% ===================================================================== %%
% jointAnglePanelQ1Val_Callback
%    Callback fired when the q1  value in changed on the joint angle panel
function jointAnglePanelQ1Val_Callback(hObject, eventdata, handles)
% Update GUI to show new value on slider
handles.jointAnglePanelQ1Slider.Value = str2double(handles.jointAnglePanelQ1Val.String);

% jointAnglePanelQ2Val_Callback
%    Callback fired when the q1  value in changed on the joint angle panel
function jointAnglePanelQ2Val_Callback(hObject, eventdata, handles)
% Update GUI to show new value on slider
handles.jointAnglePanelQ2Slider.Value = str2double(handles.jointAnglePanelQ2Val.String);

% jointAnglePanelQ3Val_Callback
%    Callback fired when the q1  value in changed on the joint angle panel
function jointAnglePanelQ3Val_Callback(hObject, eventdata, handles)
% Update GUI to show new value on slider
handles.jointAnglePanelQ3Slider.Value = str2double(handles.jointAnglePanelQ3Val.String);

% jointAnglePanelQ4Val_Callback
%    Callback fired when the q1  value in changed on the joint angle panel
function jointAnglePanelQ4Val_Callback(hObject, eventdata, handles)
% Update GUI to show new value on slider
handles.jointAnglePanelQ4Slider.Value = str2double(handles.jointAnglePanelQ4Val.String);

% jointAnglePanelQ5Val_Callback
%    Callback fired when the q1  value in changed on the joint angle panel
function jointAnglePanelQ5Val_Callback(hObject, eventdata, handles)
% Update GUI to show new value on slider
handles.jointAnglePanelQ5Slider.Value = str2double(handles.jointAnglePanelQ5Val.String);

% jointAnglePanelQ6Val_Callback
%    Callback fired when the q1  value in changed on the joint angle panel
function jointAnglePanelQ6Val_Callback(hObject, eventdata, handles)
% Update GUI to show new value on slider
handles.jointAnglePanelQ6Slider.Value = str2double(handles.jointAnglePanelQ6Val.String);

% jointAnglePanelQ7Val_Callback
%    Callback fired when the q1  value in changed on the joint angle panel
function jointAnglePanelQ7Val_Callback(hObject, eventdata, handles)
% Update GUI to show new value on slider
handles.jointAnglePanelQ7Slider.Value = str2double(handles.jointAnglePanelQ7Val.String);

% jointAnglePanelQ1Slider_Callback
%    Callback fired when the q1 slider is changed on the joint angle panel
function jointAnglePanelQ1Slider_Callback(hObject, eventdata, handles)
% Update GUI to show new value in val box
handles.jointAnglePanelQ1Val.String = string(handles.jointAnglePanelQ1Slider.Value);
moveRobotToJointAngles(handles);

% jointAnglePanelQ2Slider_Callback
%    Callback fired when the q2 slider is changed on the joint angle panel
function jointAnglePanelQ2Slider_Callback(hObject, eventdata, handles)
% Update GUI to show new value in val box
handles.jointAnglePanelQ2Val.String = string(handles.jointAnglePanelQ2Slider.Value);
moveRobotToJointAngles(handles);

% jointAnglePanelQ3Slider_Callback
%    Callback fired when the q3 slider is changed on the joint angle panel
function jointAnglePanelQ3Slider_Callback(hObject, eventdata, handles)
% Update GUI to show new value in val box
handles.jointAnglePanelQ3Val.String = string(handles.jointAnglePanelQ3Slider.Value);
moveRobotToJointAngles(handles);

% jointAnglePanelQ4Slider_Callback
%    Callback fired when the q4 slider is changed on the joint angle panel
function jointAnglePanelQ4Slider_Callback(hObject, eventdata, handles)
% Update GUI to show new value in val box
handles.jointAnglePanelQ4Val.String = string(handles.jointAnglePanelQ4Slider.Value);
moveRobotToJointAngles(handles);

% jointAnglePanelQ5Slider_Callback
%    Callback fired when the q5 slider is changed on the joint angle panel
function jointAnglePanelQ5Slider_Callback(hObject, eventdata, handles)
% Update GUI to show new value in val box
handles.jointAnglePanelQ5Val.String = string(handles.jointAnglePanelQ5Slider.Value);
moveRobotToJointAngles(handles);

% jointAnglePanelQ6Slider_Callback
%    Callback fired when the q6 slider is changed on the joint angle panel
function jointAnglePanelQ6Slider_Callback(hObject, eventdata, handles)
% Update GUI to show new value in val box
handles.jointAnglePanelQ6Val.String = string(handles.jointAnglePanelQ6Slider.Value);
moveRobotToJointAngles(handles);

% jointAnglePanelQ7Slider_Callback
%    Callback fired when the q7 slider is changed on the joint angle panel
function jointAnglePanelQ7Slider_Callback(hObject, eventdata, handles)
% Update GUI to show new value in val box
handles.jointAnglePanelQ7Val.String = string(handles.jointAnglePanelQ7Slider.Value);
moveRobotToJointAngles(handles);

%% ===================================================================== %%
%%                       moveRobotToPose                                 %%
%   Reads the values in the cartesian space (x, y, z, roll, pitch, yaw) 
%    and moves the robot to the pose described by these points 
%% ===================================================================== %%
function moveRobotToPose(handles)
x = str2num(handles.positionPanelX.String);
y = str2num(handles.positionPanelY.String);
z = str2num(handles.positionPanelZ.String);

roll = str2num(handles.orientationPanelRoll.String);
pitch = str2num(handles.orientationPanelPitch.String);
yaw = str2num(handles.orientationPanelYaw.String);

pose = transl(x,y,z)*trotx(roll, 'deg')*troty(pitch, 'deg')*trotz(yaw , 'deg');

handles.cytonRobotOne.MoveRobotToPose(pose);

%% ===================================================================== %%
%%                       moveRobotToPosition                             %%
%   Similar to moveRobotToPose, but the orientation is ignored.           %
%   Only cartesian x,y,z are utilised.                                    %
%% ===================================================================== %%
function moveRobotToPosition(handles)
x = str2num(handles.positionPanelX.String);
y = str2num(handles.positionPanelY.String);
z = str2num(handles.positionPanelZ.String);

handles.cytonRobotOne.MoveRobotToPoint(x, y, z);
pos = rad2deg(handles.cytonRobotOne.GetPosition());

handles.jointAnglePanelQ1Slider.Value = round(pos(1),1);
handles.jointAnglePanelQ2Slider.Value = round(pos(2),1);
handles.jointAnglePanelQ3Slider.Value = round(pos(3),1);
handles.jointAnglePanelQ4Slider.Value = round(pos(4),1);
handles.jointAnglePanelQ5Slider.Value = round(pos(5),1);
handles.jointAnglePanelQ6Slider.Value = round(pos(6),1);
handles.jointAnglePanelQ7Slider.Value = round(pos(7),1);

handles.jointAnglePanelQ1Val.String = num2str(round(pos(1),1));
handles.jointAnglePanelQ2Val.String = num2str(round(pos(2),1));
handles.jointAnglePanelQ3Val.String = num2str(round(pos(3),1));
handles.jointAnglePanelQ4Val.String = num2str(round(pos(4),1));
handles.jointAnglePanelQ5Val.String = num2str(round(pos(5),1));
handles.jointAnglePanelQ6Val.String = num2str(round(pos(6),1));
handles.jointAnglePanelQ7Val.String = num2str(round(pos(7),1));

%% ===================================================================== %%
%%                       moveRobotToJointAngles                          %%
%   Moves robot to joint angles specified by the joint angle sliders      %
%% ===================================================================== %%
function moveRobotToJointAngles(handles)

q_mat = [];
q_mat(1) = handles.jointAnglePanelQ1Slider.Value;
q_mat(2) = handles.jointAnglePanelQ2Slider.Value;
q_mat(3) = handles.jointAnglePanelQ3Slider.Value;
q_mat(4) = handles.jointAnglePanelQ4Slider.Value;
q_mat(5) = handles.jointAnglePanelQ5Slider.Value;
q_mat(6) = handles.jointAnglePanelQ6Slider.Value;
q_mat(7) = handles.jointAnglePanelQ7Slider.Value;

pos = handles.cytonRobotOne.model.fkine(q_mat);
cart = pos(1:3,4)';
[orient_x,orient_y,orient_z] = decomposeRotation(pos(1:3,1:3))

updateGuiOrientation(handles, orient_x, orient_y, orient_z);
updateGuiPos(handles, cart);

handles.cytonRobotOne.MoveRobotToJointAnglesSmall(deg2rad(q_mat));

%% ===================================================================== %%
%%                       decomposeRotation                               %%
%   Extracts roll, pitch & yaw from a homogenous 4x4 matrix               %
%% ===================================================================== %%
function [x,y,z] = decomposeRotation(R)
x = rad2deg(atan2(R(3,2), R(3,3)));
y = rad2deg(atan2(-R(3,1), sqrt(R(3,2)*R(3,2) + R(3,3)*R(3,3))));
z = rad2deg(atan2(R(2,1), R(1,1)));

%% ===================================================================== %%
%%                       updateGuiOrientation                            %%
%   Updates GUI orientation panels with specified roll, pitch, and yaw    %
%% ===================================================================== %%
function updateGuiOrientation(handles, orient_x, orient_y, orient_z)
handles.orientationPanelRoll.String  = string(round(orient_x,3));
handles.orientationPanelPitch.String = string(round(orient_y,3));
handles.orientationPanelYaw.String   = string(round(orient_z,3));

%% ===================================================================== %%
%%                       updateGuiPos                                    %%
%   Updates GUI position panels with specified cartesian coordinates      %
%% ===================================================================== %%
function updateGuiPos(handles, cart)
handles.positionPanelX.String = string(round(cart(1),3));
handles.positionPanelY.String = string(round(cart(2),3));
handles.positionPanelZ.String = string(round(cart(3),3));

%% ===================================================================== %%
%%                      Hand position callbacks                          %%
%   These callbacks define hand behaviour                                 %
%% ===================================================================== %%
% --- Executes on slider movement.
function handPositionPanelXSlider_Callback(hObject, eventdata, handles)
xPos = handles.handPositionPanelXSlider.Value;

handles.robotHand.model.base = transl(xPos,0,0.1);
handles.robotHand.PlotAndColourRobot();

%% ===================================================================== %%
%%              Coffee Pod Position callbacks                            %%
%   These control the position of the coffee pods                        %%
%% ===================================================================== %%
% RCpPanelPosx_Callback
%   Callback fired when x value for red coffee pod changed 
function RCpPanelPosx_Callback(hObject, eventdata, handles)
updateRCpPos(handles)

% RCpPanelPosy_Callback
%   Callback fired when y value for red coffee pod changed 
function RCpPanelPosy_Callback(hObject, eventdata, handles)
updateRCpPos(handles)

% RCpPanelPosz_Callback
%   Callback fired when z value for red coffee pod changed 
function RCpPanelPosz_Callback(hObject, eventdata, handles)
updateRCpPos(handles)

% BCpPanelPosx_Callback
%   Callback fired when x value for blue coffee pod changed 
function BCpPanelPosx_Callback(hObject, eventdata, handles)
updateBCpPos(handles)

% BCpPanelPosy_Callback
%   Callback fired when y value for blue coffee pod changed 
function BCpPanelPosy_Callback(hObject, eventdata, handles)
updateBCpPos(handles)

% BCpPanelPosz_Callback
%   Callback fired when z value for blue coffee pod changed 
function BCpPanelPosz_Callback(hObject, eventdata, handles)
updateBCpPos(handles)

% GCpPanelPosx_Callback
%   Callback fired when x value for green coffee pod changed 
function GCpPanelPosx_Callback(hObject, eventdata, handles)
updateGCpPos(handles);

% GCpPanelPosy_Callback
%   Callback fired when y value for green coffee pod changed 
function GCpPanelPosy_Callback(hObject, eventdata, handles)
updateGCpPos(handles);

% GCpPanelPosz_Callback
%   Callback fired when z value for green coffee pod changed 
function GCpPanelPosz_Callback(hObject, eventdata, handles)
updateGCpPos(handles);

% updateRCpPos
%   updates position of red coffee pod using cartesian coordinates
%   specified in GUI 
function updateRCpPos(handles)
x = round(str2double(handles.RCpPanelPosx.String),3);
y = round(str2double(handles.RCpPanelPosy.String),3);
z = round(str2double(handles.RCpPanelPosz.String),3);
handles.redPod.SetPos(transl(x,y,z));

% updateBCpPos
%   updates position of blue coffee pod using cartesian coordinates
%   specified in GUI
function updateBCpPos(handles)
x = round(str2double(handles.BCpPanelPosx.String),3);
y = round(str2double(handles.BCpPanelPosy.String),3);
z = round(str2double(handles.BCpPanelPosz.String),3);
handles.bluePod.SetPos(transl(x,y,z));

% updateGCpPos
%   updates position of green coffee pod using cartesian coordinates
%   specified in GUI
function updateGCpPos(handles)
x = round(str2double(handles.GCpPanelPosx.String),3);
y = round(str2double(handles.GCpPanelPosy.String),3);
z = round(str2double(handles.GCpPanelPosz.String),3);
handles.greenPod.SetPos(transl(x,y,z));

%% ===================================================================== %%
%%                      End effector Functions                           %%
%   These are callbacks & functions for the end effector                 %%
%% ===================================================================== %%
% --- Executes on button press in endEffectorPanelEndEffector.
function endEffectorPanelEndEffector_Callback(hObject, eventdata, handles)

%% ===================================================================== %%
%%                      Simulation Functions                             %%
%   These are callbacks that update the simulation  s                     %
%% ===================================================================== %%
% --- Executes on button press in updateFromRobot.
function updateFromRobot_Callback(hObject, eventdata, handles)
updateRobotFromHw(handles);

% updateRobotFromHw
%   update simulation robot from robot hardware 
function updateRobotFromHw(handles)

handles.cytonRobotOne.UpdateSimFromHw();
pos = handles.cytonRobotOne.GetPosition();

pos = rad2deg(handles.cytonRobotOne.GetPosition());
handles.jointAnglePanelQ1Slider.Value = round(pos(1),1);
handles.jointAnglePanelQ2Slider.Value = round(pos(2),1);
handles.jointAnglePanelQ3Slider.Value = round(pos(3),1);
handles.jointAnglePanelQ4Slider.Value = round(pos(4),1);
handles.jointAnglePanelQ5Slider.Value = round(pos(5),1);
handles.jointAnglePanelQ6Slider.Value = round(pos(6),1);
handles.jointAnglePanelQ7Slider.Value = round(pos(7),1);

handles.jointAnglePanelQ1Val.String = num2str(round(pos(1),1));
handles.jointAnglePanelQ2Val.String = num2str(round(pos(2),1));
handles.jointAnglePanelQ3Val.String = num2str(round(pos(3),1));
handles.jointAnglePanelQ4Val.String = num2str(round(pos(4),1));
handles.jointAnglePanelQ5Val.String = num2str(round(pos(5),1));
handles.jointAnglePanelQ6Val.String = num2str(round(pos(6),1));
handles.jointAnglePanelQ7Val.String = num2str(round(pos(7),1));

%% ===================================================================== %%
%%                      jointAnglePanelShowMe_Callback                   %%
%   Takes joint angles from sliders and moves simulation to that          %
%   position using jtraj                                                  %
%% ===================================================================== %%
function jointAnglePanelShowMe_Callback(hObject, eventdata, handles)

q_mat = [];
q_mat(1) = handles.jointAnglePanelQ1Slider.Value;
q_mat(2) = handles.jointAnglePanelQ2Slider.Value;
q_mat(3) = handles.jointAnglePanelQ3Slider.Value;
q_mat(4) = handles.jointAnglePanelQ4Slider.Value;
q_mat(5) = handles.jointAnglePanelQ5Slider.Value;
q_mat(6) = handles.jointAnglePanelQ6Slider.Value;
q_mat(7) = handles.jointAnglePanelQ7Slider.Value;

handles.cytonRobotOne.MoveRobotToJointAnglesPlan(deg2rad(q_mat));

pos_mat = handles.cytonRobotOne.model.fkine(handles.cytonRobotOne.GetPosition());

pos = pos_mat(1:3,4)';
orient = pos_mat(1:3,1:3);
[orient_x, orient_y, orient_z] =  decomposeRotation(orient);
updateGuiPos(handles, pos);
updateGuiOrientation(handles, orient_x, orient_y, orient_z);

%% ===================================================================== %%
%%                      jointAnglePanelDoIt_Callback                     %%
%   Similar to jointAnglePanelShowMe_Callback, except the actual robot    %
%   moves                                                                 %
%% ===================================================================== %%
% --- Executes on button press in jointAnglePanelDoIt.
function jointAnglePanelDoIt_Callback(hObject, eventdata, handles)
q_mat = [];
q_mat(1) = handles.jointAnglePanelQ1Slider.Value;
q_mat(2) = handles.jointAnglePanelQ2Slider.Value;
q_mat(3) = handles.jointAnglePanelQ3Slider.Value;
q_mat(4) = handles.jointAnglePanelQ4Slider.Value;
q_mat(5) = handles.jointAnglePanelQ5Slider.Value;
q_mat(6) = handles.jointAnglePanelQ6Slider.Value;
q_mat(7) = handles.jointAnglePanelQ7Slider.Value;
q_mat = deg2rad(q_mat);

handles.cytonRobotOne.MoveRobotToJointAngles(q_mat);


%% ===================================================================== %%
%%                      kys_Callback                                     %%
%   Initially implemented so the robot would E-stop itself.               %
%% ===================================================================== %%
function kys_Callback(hObject, eventdata, handles)
% hObject    handle to kys (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.cytonRobotOne.MoveHw();

%% ===================================================================== %%
%%                      Create Functions                                 %%
%   These are auto generated & we don't care about them for this         %%
%   assignment                                                           %%
%% ===================================================================== %%
% --- Executes during object creation, after setting all properties.
function orientationPanelRoll_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function orientationPanelPitch_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function orientationPanelYaw_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function positionPanelX_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function positionPanelY_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function positionPanelZ_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function jointAnglePanelQ1Slider_CreateFcn(hObject, eventdata, handles)
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

% --- Executes during object creation, after setting all properties.
function jointAnglePanelQ2Slider_CreateFcn(hObject, eventdata, handles)
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

% --- Executes during object creation, after setting all properties.
function jointAnglePanelQ3Slider_CreateFcn(hObject, eventdata, handles)
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

% --- Executes during object creation, after setting all properties.
function jointAnglePanelQ4Slider_CreateFcn(hObject, eventdata, handles)
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

% --- Executes during object creation, after setting all properties.
function jointAnglePanelQ5Slider_CreateFcn(hObject, eventdata, handles)
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

% --- Executes during object creation, after setting all properties.
function jointAnglePanelQ6Slider_CreateFcn(hObject, eventdata, handles)
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

% --- Executes during object creation, after setting all properties.
function jointAnglePanelQ7Slider_CreateFcn(hObject, eventdata, handles)
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

% --- Executes during object creation, after setting all properties.
function jointAnglePanelQ1Val_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function jointAnglePanelQ2Val_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function jointAnglePanelQ3Val_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function jointAnglePanelQ4Val_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function jointAnglePanelQ5Val_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function jointAnglePanelQ6Val_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function jointAnglePanelQ7Val_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function RCpPanelPosx_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function RCpPanelPosy_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function RCpPanelPosz_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function BlackCpPanelPosx_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function BlackCpPanelPosy_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function BlackCpPanelPosz_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function BCpPanelPosx_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function BCpPanelPosy_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function BCpPanelPosz_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function GCpPanelPosx_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function GCpPanelPosy_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function GCpPanelPosz_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function handPositionPanelXSlider_CreateFcn(hObject, eventdata, handles)
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end
