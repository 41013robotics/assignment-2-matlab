classdef CytonROS < handle
    properties
        % Message and Service Properties
        cute_enable_robot_client
        cute_enable_robot_msg
        cute_move_client
        cute_enable_gripper_client
        cute_enable_gripper_msg
        cute_execute_client
        cute_move_msg
        cute_execute_msg
        cute_multi_joint_client
        cute_multi_joint_msg
        cute_move_all_msg
        cute_claw_publisher
        cute_claw_msg
        cute_jointAngles_msg
        stateSub
        
        % Q States - Hardcoded safe locations.
        qHome = [0 0 0 0 0 0 0];
        q45 = deg2rad([45 45 45 45 45 45 45]);
        qNeg30 = deg2rad([-30 -30 -30 -30 -30 -30 -30]);
        q90 = deg2rad([90 90 90 90 90 -90 90]);
        
        qCoffee = [    1.6260   -0.0951   -0.2562   -0.5185    1.9221   -0.6719   -0.7409];
        
        qLeftHover = [0.7302    1.0753    0.2976    0.7133    0.7900    1.2226    0.0491];
        qLeft = [0.7701 1.2026 0.3068 0.6796 0.7685 1.1152 0.1104];
        
        qMiddleHover = [0.0752    1.0661   -0.7256    0.5001   -1.7625    0.8590   -1.7257];
        qMiddle = [ 0.1381    1.2210   -0.7225    0.5354   -1.5739    0.8974   -1.7763];
        
        qRightHover = [    0.0061    1.0922    0.9158    0.3543    1.8270    0.7102   -1.4435];
        qRight = [    0.0138    1.2241    0.9158    0.3543    1.7334    0.7102   -1.4358];
        
        
    end
    
    methods
        %% MAIN
        function self = CytonROS()
            
            self.CytonInit(); %start all of the services
            self.CytonJoints(self.qHome); %Go to safe upright position
            self.CytonMoveJoints(); 
        end
        
        %%
        function CytonInit(self)
            
            self.cute_enable_robot_client = rossvcclient('cute_enable_robot');
            self.cute_enable_robot_msg = rosmessage(self.cute_enable_robot_client);
            display('Robot Client Created')
            
            self.cute_enable_gripper_client = rossvcclient('claw_controller/torque_enable');
            self.cute_enable_gripper_msg = rosmessage(self.cute_enable_gripper_client);
            display('Robot Claw Client Created')
            
            self.cute_enable_robot_msg.EnableRobot = true; %false
            self.cute_enable_robot_client.call(self.cute_enable_robot_msg);
            display('Robot Motors Enabled')
            
            self.cute_enable_gripper_msg.TorqueEnable = true;% false
            self.cute_enable_gripper_client.call(self.cute_enable_gripper_msg);
            self.cute_claw_publisher = rospublisher('/claw_controller/command');
            self.cute_claw_msg = rosmessage(self.cute_claw_publisher);
            display('Robot Claw Enabled')
            
            self.cute_multi_joint_client = rossvcclient('/cute_multi_joint');
            self.cute_multi_joint_msg = rosmessage(self.cute_multi_joint_client);
            display('Robot Joint Controll Enabled')
            
            self.stateSub = rossubscriber('/joint_states');
            receive(self.stateSub,2);
            msg = self.stateSub.LatestMessage;
            display('Robot Connected to Joint States')
            
            self.cute_move_client = rossvcclient('/cute_move');
            self.cute_move_msg = rosmessage(self.cute_move_client);
            self.cute_execute_client = rossvcclient('/cute_execute_plan');
            self.cute_execute_msg = rosmessage(self.cute_execute_client);
            display('Robot Ready to Move')
            
            self.cute_multi_joint_msg.Vel = 1; %set to max safe velocity.
            display('Velocity has been set to 1');
        end
        %%
        function CytonOff(self)
            % Turn off the services and deenergise the motors.
            self.cute_enable_robot_client = rossvcclient('cute_enable_robot')
            self.cute_enable_robot_msg = rosmessage(self.cute_enable_robot_client);
            self.cute_enable_robot_msg.EnableRobot = false
            self.cute_enable_robot_client.call(self.cute_enable_robot_msg);
            display('Robot Motors Disabled')
            
            self.cute_enable_gripper_client = rossvcclient('claw_controller/torque_enable');
            self.cute_enable_gripper_msg = rosmessage(self.cute_enable_gripper_client);
            self.cute_enable_gripper_msg.TorqueEnable = false;
            self.cute_enable_gripper_client.call(self.cute_enable_gripper_msg);
            display('Robot Claw Disabled')
        end
        %%
        function CytonPos(self, PosX, PosY, PosZ)
            %Set the cartesian position.
            self.cute_move_msg.Pose.Position.X = PosX;
            self.cute_move_msg.Pose.Position.Y = PosY;
            self.cute_move_msg.Pose.Position.Z = PosZ;
            display(['Robot End Effector Set To: (',num2str(PosX), ' ,', num2str(PosY), ' ,' ,num2str(PosZ), ')'])
            
            quat = eul2quat([0,pi,0]);%Remember: Read the help file to understand how to use this function (’help eul2quat’)
            self.cute_move_msg.Pose.Orientation.W = quat(1);
            self.cute_move_msg.Pose.Orientation.X = quat(2);
            self.cute_move_msg.Pose.Orientation.Y = quat(3);
            self.cute_move_msg.Pose.Orientation.Z = quat(4);
        end
        %%
        function CytonMovePos(self)
            %Go to the cartesian position that is saved in cute_move_msg)
            response = self.cute_move_client.call(self.cute_move_msg);
            self.cute_execute_msg.JointTrajectory = response.JointTrajectory;
            self.cute_execute_client.call(self.cute_execute_msg);
            display('Robot Position Movement Completed')
            %TO ADD ROBOT FINISHED MOVEMENT HERE
            
            %         end
        end
        %%
        function CytonJoints(self, q)
            %set the q states of each joint for the robot to go to.
            self.cute_multi_joint_msg.JointStates = q;
            display(['Robot Joint States Set to: (',num2str(q), ')'])
            
        end
        %%
        function CytonMoveJoints(self)
            %go to the joint states saved in cute_multi_joint_msg
            self.cute_multi_joint_client.call(self.cute_multi_joint_msg);
            display('Robot Joint Movement Completed')
            
            
        end
        %%
        function CytonClawState(self, value) %-1.5 close 0 open
            self.cute_claw_msg.Data = value; % Values must be between -1.5 (closed) and 0 (open)
            self.cute_claw_publisher.send(self.cute_claw_msg);
            display(['Robot Claw State: ',num2str(value)])
        end
        
        %%
        function jointAngles = GetJointAngles(self)
            %returns the joint state - used to work out the safe hardcoded
            %position.
            jointAngles = self.stateSub.LatestMessage.Position;
            jointAngles = jointAngles(1:7,:)';
        end
        
    end
end
