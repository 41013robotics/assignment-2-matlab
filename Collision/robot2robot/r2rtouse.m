function result = RobotCollision(robot1,qMatrix1,robot2,qMatrix2)

result = false;
% % %
% % % for qIndex = 1:size(qMatrix1,1)
% % %     q = qMatrix1(qIndex,:);
% Get the transform of every joint (i.e. start and end of every link)
if (robot1.n >= robot2.n)
    bigger = robot1.n;
    smaller = robot2.n;
else
    bigger = robot2.n;
    smaller = robot1.n;
end

tr1 = zeros(4,4,robot1.n+1);
tr1(:,:,1) = robot1.base;
L1 = robot1.links;
tr2 = zeros(4,4,robot2.n+1);
tr2(:,:,1) = robot2.base;
L2 = robot2.links;


for i = 1 : bigger
    tr1(:,:,i+1) = tr1(:,:,i) * trotz(q(i)+L1(i).offset) * transl(0,0,L1(i).d) * transl(L1(i).a,0,0) * trotx(L1(i).alpha);
end
for i = 1 : smaller
    tr2(:,:,i+1) = tr2(:,:,i) * trotz(q(i)+L2(i).offset) * transl(0,0,L2(i).d) * transl(L2(i).a,0,0) * trotx(L2(i).alpha);
end

% Go through each link and also each triangle face
% WHEN q = [0,0,0];
for i = 1 : bigger - 1 % might not be -1
    for j = 1 : smaller
        result = false;
        PA = zeros(2,2);
        PB = zeros(2,2);
        PA(1,:) = tr1(1:3,4,i);
        PA(2,:) = tr2(1:3,4,j);
        PB(1,:) = tr1(1:3,4,i+1);
        PB(2,:) = tr2(1:3,4,j+1);
        result = lineIntersect3D(PA,PB);
    end
end
end

function [...P_intersect,distances
            result] = lineIntersect3D(PA,PB)
% Find intersection point of lines in 3D space, in the least squares sense.
% PA :          Nx3-matrix containing starting point of N lines
% PB :          Nx3-matrix containing end point of N lines
% P_Intersect : Best intersection point of the N lines, in least squares sense.
% distances   : Distances from intersection point to the input lines
% Anders Eikenes, 2012

Si = PB - PA; %N lines described as vectors
ni = Si ./ (sqrt(sum(Si.^2,2))*ones(1,3)); %Normalize vectors
nx = ni(:,1); ny = ni(:,2); nz = ni(:,3);
SXX = sum(nx.^2-1);
SYY = sum(ny.^2-1);
SZZ = sum(nz.^2-1);
SXY = sum(nx.*ny);
SXZ = sum(nx.*nz);
SYZ = sum(ny.*nz);
S = [SXX SXY SXZ;SXY SYY SYZ;SXZ SYZ SZZ];
CX  = sum(PA(:,1).*(nx.^2-1) + PA(:,2).*(nx.*ny)  + PA(:,3).*(nx.*nz));
CY  = sum(PA(:,1).*(nx.*ny)  + PA(:,2).*(ny.^2-1) + PA(:,3).*(ny.*nz));
CZ  = sum(PA(:,1).*(nx.*nz)  + PA(:,2).*(ny.*nz)  + PA(:,3).*(nz.^2-1));
C   = [CX;CY;CZ];
P_intersect = (S\C)';

if nargout>1
    N = size(PA,1);
    %distances=zeros(N,1);
    for i=1:N %This is faster:
        ui=(P_intersect-PA(i,:))*Si(i,:)'/(Si(i,:)*Si(i,:)');
        %distances(i)=norm(P_intersect-PA(i,:)-ui*Si(i,:));
        if norm(P_intersect-PA(i,:)-ui*Si(i,:)) == 0
            result = true;
        end
    end
    %for i=1:N %http://mathworld.wolfram.com/Point-LineDistance3-Dimensional.html:
    %    distances(i) = norm(cross(P_intersect-PA(i,:),P_intersect-PB(i,:))) / norm(Si(i,:));
    %end
end
end
