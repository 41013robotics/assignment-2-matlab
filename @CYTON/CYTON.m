classdef CYTON < handle
    properties
        %> Robot model
        model;
        name;
        %> workspace
        workspace = [-0.8 0.8 -0.8 0.8 0 0.7];
        
        %> If we have a tool model which will replace the final links model, combined ply file of the tool model and the final link models
        toolModelFilename = []; % Available are: 'DabPrintNozzleTool.ply';
        toolParametersFilename = []; % Available are: 'DabPrintNozzleToolParameters.mat';
        
        robot_hardware;
        h;
        handles;
        
        steps = 21; %set to 21 because matlab cells start from 1. The simulation steps are for all 21 steps - the real robot is for every 5th step.
        
    end
    
    methods%% Class for CYTON robot simulation
        function self = CYTON()
            % Create Cyton robot
            self.GetCYTONRobot()
        end
        
        %% GetCYTONRobot
        % Given a name (optional), create and return a CYTON robot model
        function GetCYTONRobot(self)
            pause(0.001);
            self.name = ['CYTON_',datestr(now,'yyyymmddTHHMMSSFFF')];
            L1 = Link('d',0.1491+0.009,'a',0,          'alpha',-pi/2,   'offset',0,'qlim',deg2rad([-180,180]));
            L2 = Link('d',0,   'a',0, 'alpha',-pi/2,      'offset',pi,'qlim',deg2rad([-180,180]));
            L3 = Link('d',0.1255,    'a',0,          'alpha',pi/2,  'offset',0,'qlim',deg2rad([-180,180]));
            L4 = Link('d',0,   'a',-0.0665 ,          'alpha',pi/2,   'offset',-pi/2,'qlim',deg2rad([-180,180]));
            L5 = Link('d',0,    'a',-0.0665,          'alpha',-pi/2,  'offset',0,'qlim',deg2rad([-180,180]));
            L6 = Link('d',0,    'a',0,          'alpha',pi/2,  'offset',-pi/2,'qlim',deg2rad([-180,180]));
            L7 = Link('d',0.027,    'a',0,          'alpha',0,  'offset',0,'qlim',deg2rad([-180,180]));
            
            self.model = SerialLink([L1 L2 L3 L4 L5 L6 L7],'name',self.name);
            
            try
                self.robot_hardware = CytonROS();
            catch
                warning("Couldn't initialise robot. You likely don't have all the ROS components installed"); %lets users run the GUI without a rosmaster
            end
        end
        
        %% PlotAndColourRobot
        % Given a robot index, add the glyphs (vertices and faces) and
        % colour them in if data is available
        function PlotAndColourRobot(self)%robot,workspace)
            
            for linkIndex = 0:self.model.n
                [ faceData, vertexData, plyData{linkIndex + 1} ] = plyread(['CYTONLink',num2str(linkIndex),'.ply'],'tri'); %#ok<AGROW>
                self.model.faces{linkIndex + 1} = faceData;
                self.model.points{linkIndex + 1} = vertexData;
            end
            
            if ~isempty(self.toolModelFilename)
                [ faceData, vertexData, plyData{self.model.n + 1} ] = plyread(self.toolModelFilename,'tri');
                self.model.faces{self.model.n + 1} = faceData;
                self.model.points{self.model.n + 1} = vertexData;
                toolParameters = load(self.toolParametersFilename);
                self.model.tool = toolParameters.tool;
                self.model.qlim = toolParameters.qlim;
                warning('Please check the joint limits. They may be unsafe')
            end
            %            Display robot
            self.model.plot3d(zeros(1,self.model.n),'noarrow','workspace',self.workspace);
            if isempty(findobj(get(gca,'Children'),'Type','Light'))
                camlight
            end
            self.model.delay = 0;
            
            %             % Try to correctly colour the arm (if colours are in ply file data)
            for linkIndex = 0:self.model.n
                self.handles = findobj('Tag', self.model.name);
                self.h = get(self.handles,'UserData');
                try
                    self.h.link(linkIndex+1).Children.FaceVertexCData = [plyData{linkIndex+1}.vertex.red ...
                        , plyData{linkIndex+1}.vertex.green ...
                        , plyData{linkIndex+1}.vertex.blue]/255;
                    self.h.link(linkIndex+1).Children.FaceColor = 'interp';
                catch ME_1
                    disp(ME_1);
                    continue;
                end
            end
        end
        %% Animate Robot 
        % Update the simulation with a qMatrix
        function AnimateRobot(self, angles)
            for i = 1:numel(angles(:,1))
                animate(self.model, angles(i,:));
                disp(angles(i,:));
                pause(0.1);
                drawnow;
            end
        end
        %% MoveRobotToPoint
        % Plans a path and moves robot to a point
        function MoveRobotToPoint(self, x, y, z)
            
            % Get the current joint angles
            q1 = self.model.getpos();
            %Solve the inverse kinematics to get the required joint angles
            q2 = self.model.ikcon(transl(x,y,z), q1);
            %Define the number of steps
            
            self.steps = 21;
            
            pos_matrix = jtraj(q1, q2, self.steps);
            
            self.AnimateRobot(pos_matrix);
        end
        
        %% MoveRobotToPose
        % Plans a path and moves robot to a point
        function MoveRobotToPose(self, pose)
            
            % Get the current joint angles
            q1 = self.robot_hardware.GetJointAngles();
            %Solve the inverse kinematics to get the required joint angles
            q2 = self.model.ikcon(pose); 
            %Define the number of steps
            self.steps = 21;
            
            self.MoveRobotToJointAnglesSmall(q2); 
        end
%         
        %% MoveRobotToJointAnglesPlan
        % Plans a path and moves robot to a point
        
        function MoveRobotToJointAnglesPlan(self, angles)
            
            % Get the current joint angles
            q1 = self.robot_hardware.GetJointAngles();
            %Solve the inverse kinematics to get the required joint angles
            q2 = angles;
            %Define the number of steps
            self.steps = 21;
            
            %             pos_matrix = jtraj(q1, q2, self.steps);
            qMatrix = self.JtrajSolver(q1,q2);
            
        end
        function MoveRobotToJointAnglesSmall(self, angles)
           
             % Get the current joint angles
            q1 = self.robot_hardware.GetJointAngles();
            q2 = angles; %from gui
            %Define the number of steps
            self.steps = 21;
            
            animate(self.model, q2);
        end
        %% MoveRobotToJointAngles
        % Moves robot to teach angles
        function MoveRobotToJointAngles(self, angles)
            
            % Get the current joint angles
            q1 = self.robot_hardware.GetJointAngles();
            q2 = angles; %from GUI
            %Define the number of steps

            qMatrix = self.JtrajSolver(q1,q2);
            self.RunMovements(qMatrix);
        end
        %% Get Position
        function pos = GetPosition(self)
            pos = self.model.getpos(); %returns position of robot in simulation
        end

        %% Jtraj solver with 5 steps
        % solve the movements from one joint state to another.
        function qMatrix = JtrajSolver(self, q1, q2)
            self.steps = 21;
            qMatrix = jtraj(q1, q2, self.steps);
            self.AnimateRobot(qMatrix); %visualise
        end
        %% Run Movements
        %send movements to robot hardware
        function error = RunMovements(self, q, handles)
            error = 1;
            for i=1:5:self.steps
                self.robot_hardware.CytonJoints(q(i,:));
                self.robot_hardware.CytonMoveJoints();
            end
        end
        %% updateSimFromHw
        % Update simulation from hardware
        function UpdateSimFromHw(self)
            if ~(isempty(self.robot_hardware))
                q = self.robot_hardware.GetJointAngles()
                self.MoveRobotToJointAngles(q(1:7)');
            else
                warning("Robot hardware not initialised")
            end
        end
        %% Making Coffee Jtrajs
        function MoveHw(self)
            %hard coded trajectory solving
            %creates a qMatrix for each waypoint.
            qMatrix{1} = self.JtrajSolver(self.robot_hardware.GetJointAngles(), self.robot_hardware.qHome)
            qMatrix{2}  = self.JtrajSolver(self.robot_hardware.qHome, self.robot_hardware.qLeftHover)
            qMatrix{3}  = self.JtrajSolver(self.robot_hardware.qLeftHover, self.robot_hardware.qLeft)
            qMatrix{4}  = self.JtrajSolver(self.robot_hardware.qLeft, self.robot_hardware.qLeftHover)
            qMatrix{5}  = self.JtrajSolver(self.robot_hardware.qLeftHover, self.robot_hardware.qCoffee)
            
            
            qMatrix{6}  = self.JtrajSolver(self.robot_hardware.qCoffee, self.robot_hardware.qMiddleHover)
            qMatrix{7}  = self.JtrajSolver(self.robot_hardware.qMiddleHover, self.robot_hardware.qMiddle)
            qMatrix{8}  = self.JtrajSolver(self.robot_hardware.qMiddle, self.robot_hardware.qMiddleHover)
            qMatrix{9}  = self.JtrajSolver(self.robot_hardware.qMiddleHover, self.robot_hardware.qCoffee)
            
            qMatrix{10}  = self.JtrajSolver(self.robot_hardware.qCoffee, self.robot_hardware.qRightHover)
            qMatrix{11}  = self.JtrajSolver(self.robot_hardware.qRightHover, self.robot_hardware.qRight)
            qMatrix{12}  = self.JtrajSolver(self.robot_hardware.qRight, self.robot_hardware.qRightHover)
            qMatrix{13}  = self.JtrajSolver(self.robot_hardware.qRightHover, self.robot_hardware.qCoffee)
            qMatrix{14}  = self.JtrajSolver(self.robot_hardware.qCoffee, self.robot_hardware.qHome)

            
            display('Press a key to continue if the simulation was safe')
            pause
            
            self.robot_hardware.CytonClawState(0);
            
            for i = 1:1:14 %go through qMatrices
                self.RunMovements(qMatrix{i});
                if i == 3 | i == 7 | i == 11 %close claw at certain waypoints.
                    self.robot_hardware.CytonClawState(-0.5);
                    display('Opening Claw')
                end
                if i == 5 | i == 9 | i == 13
                    self.robot_hardware.CytonClawState(0); %open claw at certain waypoints.
                    pause(0.5)
                    display('Closing Claw')
                end
            end
            
 
        end
    end
end