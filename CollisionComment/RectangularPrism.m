function [vertex,face,faceNormals] = RectangularPrism(side, centerpnt,plotOptions,axis_h)
%This code was taken from the UTS Robotics tutorials and edited to allow
%visual lines to show where the rectangular Prism is formed in space

lower = [centerpnt(1)-side/2,centerpnt(2)-side/2,centerpnt(1)] ; %To choose the bottom corner, remember Z height of the center point is the base of the robot this no height alternation needed
upper = centerpnt+side/2; %x y Z values all added by side/2 to get top right, side should have been determined by a value greater than the robot's reach to prevent the arm from triggering light curtain during movement
if nargin<4
        axis_h=gca;
    if nargin<3 % this is where you state what you want to plot
        plotOptions.plotVerts=false;
        plotOptions.plotEdges=true;
        plotOptions.plotFaces=true;
    end
end
hold on

vertex(1,:)=lower; %xyz from lower 
vertex(2,:)=[upper(1),lower(2:3)]; %x from upper, yz from lower 
vertex(3,:)=[upper(1:2),lower(3)]; %xy from upper z from lower 
vertex(4,:)=[upper(1),lower(2),upper(3)];
vertex(5,:)=[lower(1),upper(2:3)];
vertex(6,:)=[lower(1:2),upper(3)];
vertex(7,:)=[lower(1),upper(2),lower(3)];
vertex(8,:)=upper;

face=[1,2,3;1,3,7; %each vertex from the previous lines is a point on the prism, we need to create triangles to generate the prism (2 per face, 6 faces so 12 triangles)
     1,6,5;1,7,5;
     1,6,4;1,4,2;
     6,4,8;6,5,8;
     2,4,8;2,3,8;
     3,7,5;3,8,5]; %12 triangles have been made 1-8 are correlated to the vertex values

if 2 < nargout    
    faceNormals = zeros(size(face,1),3); %create 12 normals 
    for faceIndex = 1:size(face,1)
        v1 = vertex(face(faceIndex,1)',:); %retrieve xyz from vetex1-8 depending on which faceIndex (in the for loop) eg 5 loop is face row 5.
        v2 = vertex(face(faceIndex,2)',:); %1,2,3 is the point correlating in the face matrix which points to vertex 1-8 to get the XYZ
        v3 = vertex(face(faceIndex,3)',:);
        faceNormals(faceIndex,:) = unit(cross(v2-v1,v3-v1)); % doing the corss product to create a normal to the plane
    end
end
%% If plot verticies %%depending of what visuals you choose in the GUI
if isfield(plotOptions,'plotVerts') && plotOptions.plotVerts
    for i=1:size(vertex,1)
        plot3(vertex(i,1),vertex(i,2),vertex(i,3),'r*');
        text(vertex(i,1),vertex(i,2),vertex(i,3),num2str(i));
    end
end

%% If you want to plot the edges %%WE USE THIS FOR DEMONSTRATION WITH RED LINES FOR VIRTUAL LIGHT CURTAIN
if isfield(plotOptions,'plotEdges') && plotOptions.plotEdges
    links=[1,2; %image the links are the lines/edges of the prism connected via the vertices
        2,3;
        7,3;
        1,7;
        1,6;
        6,5;
        5,7;
        4,8;
        5,8;
        6,4;
        4,2;
        8,3];

    for i=1:size(links,1) %plot all the links, although we also want some links in between to look like a  'Jail cell' pattern on the faces
        plot3(axis_h,[vertex(links(i,1),1),vertex(links(i,2),1)],... %these plots the normal edge lines
            [vertex(links(i,1),2),vertex(links(i,2),2)],...
            [vertex(links(i,1),3),vertex(links(i,2),3)],'r')
        numOfLines = 6;
        
        if (i == 1 || i==3) % this will check is you are looking at the front and back faces and start creating the vertical lines
            % look at the smallest x and largest x, find the difference and
            % divide by 10
            %plot the line up finding out what the lower and upper
            linesApart = abs(vertex(links(i,1),1) - vertex(links(i,2),1))/numOfLines;
            for b=1:numOfLines
                plot3(axis_h,[vertex(links(i,1),1)+(b*linesApart),vertex(links(i,1),1)+(b*linesApart)],...
                    [vertex(links(i,1),2),vertex(links(i,2),2)],...
                    [vertex(links(i,1),3),(vertex(links(i,1),3)+side/2)],'r');
            end
        end
        if (i == 2 || i==4) % this will check is you are looking at the side faces and start creating the vertical lines
            linesApart = abs(vertex(links(i,1),2) - vertex(links(i,2),2))/numOfLines;
            for b=1:numOfLines
                plot3(axis_h,[vertex(links(i,1),1),vertex(links(i,2),1)],...
                    [vertex(links(i,1),2)+(b*linesApart),vertex(links(i,1),2)+(b*linesApart)],...
                    [vertex(links(i,1),3),(vertex(links(i,1),3)+side/2)],'r');
            end
        end
       if (i == 6) % this will check is you are looking top face and start creating the horizontal lines at the top
            linesApart = abs(vertex(links(i,1),2) - vertex(links(i,2),2))/numOfLines;
            for b=1:numOfLines
                plot3(axis_h,[vertex(links(i,1),1),vertex(links(i,2),1)+side],...
                    [vertex(links(i,1),2)+(b*linesApart),vertex(links(i,1),2)+(b*linesApart)],...
                    [vertex(links(i,1),3),(vertex(links(i,1),3))],'r');
            end
        end
    end

end

%% If you want to plot the faces
if isfield(plotOptions,'plotFaces') && plotOptions.plotFaces
    tcolor = [.2 .2 .8];
    
    patch('Faces',face,'Vertices',vertex,'FaceVertexCData',tcolor,'FaceColor','flat','lineStyle','none');
end

end