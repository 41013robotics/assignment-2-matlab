classdef MeshObject < handle
    %MESHOBJECT Create and plot a mesh object. 
    %   Call MeshObject('./path/to/mesh/object.ply' to create the object. 
    %   Translate & SetPos can be used to move the object around. 
    
    properties
        f;                  % Faces 
        v;                  % Vertecies
        data;               % Data
        meshObject_h;       % Mesh object plot handle
        meshVerts;          % Mesh vertices 
        meshVertexCount;    % Number of verticies 
        vertexColours;      % Vertix colours
        meshPose            % Current pose of the mesh
        zMax;               % Maximum z of the mesh
        yMax;               % Maximum y of the mesh
        xMax;               % Maximum x of the mesh 
        Logger;             % Logger class
    end
    
    methods
        function self = MeshObject(self, filePath)
            %MESHOBJECT Construct an instance of this class
            [self.f, self.v, self.data] = plyread(filePath, 'tri');
            try
                self.vertexColours = [self.data.vertex.red, self.data.vertex.green, self.data.vertex.blue] / 255;
            end
            % Plot the trisurf
            self.meshObject_h = trisurf(self.f,self.v(:,1),self.v(:,2), self.v(:,3),'FaceVertexCData',self.vertexColours,'EdgeColor','interp','EdgeLighting','flat');
            
            self.zMax = max(self.v(:,3));
            self.yMax = max(self.v(:,2));
            self.xMax = max(self.v(:,1));
            
            % Get the vertex count
            self.meshVertexCount = size(self.v,1);

            % Move center point to origin
            midPoint = sum(self.v)/self.meshVertexCount;
            self.meshVerts = self.v - repmat(midPoint,self.meshVertexCount,1);

            % Create a transform to describe the location (at the origin, since it's centered
            self.meshPose = eye(4);           
        end
        
        %Move to absolute position 
        function SetPos(self,translation)
            %SETPOS Set the position of the meshobject 
            
            % Move the pose forward and a slight and random rotation
            self.meshPose = translation;
            updatedPoints = [self.meshPose * [self.meshVerts,ones(self.meshVertexCount,1)]']';  

            % Now update the Vertices
            self.meshObject_h.Vertices = updatedPoints(:,1:3);
            
            if ( ~(isempty(self.Logger)) )
               self.Logger.AppendString(datestr(datetime,'HH-MM-SS-FFF'));
               self.Logger.AppendMatrix(self.meshPose);  
               disp(self.meshPose);
            end
        end
        % Translate the object 
        function Translate(self,translation)
            %TRANSLATE Translate the mesh object
            
            % Move the pose forward and a slight and random rotation
            self.meshPose = self.meshPose * translation;
            updatedPoints = [self.meshPose * [self.meshVerts,ones(self.meshVertexCount,1)]']';  

            % Now update the Vertices
            self.meshObject_h.Vertices = updatedPoints(:,1:3);
            
            if ( ~(isempty(self.Logger)) )
               self.Logger.AppendString(datestr(datetime,'HH-MM-SS-FFF'));
               self.Logger.AppendMatrix(self.meshPose);  
               disp(self.meshPose);
            end
        end
        function pose = getPose(self) 
            %GETPOSE Returns the current pose of the mesh object 
            pose = self.meshPose; 
        end
        
        %% Start Logging
        function StartLogging(self, filename)
            %STARTLOGGING Starts logging the tranlsations of the mesh
            %object every time position changes 
            self.Logger = LogFile([],filename); 
        end        
    end
end

