%% Robotics

function [result] = CollisionLightCurtain(robot1, jointAngles, vertex, faces, faceNormals)
%robot1 = naame of robot, jointAngles is the 'n' layer of the qmatrix
%vertex faces facenormals are taken from rectangular prism

%Get the transform of every joint (i.e. start and end of every link)
tr = zeros(4,4,robot1.n+1); %3d matrix for each link robot.n is the number of links
tr(:,:,1) = robot1.base; %this the first "joint"
L = robot1.links; %dh parameters
result = IsCollision(robot1,jointAngles,faces,vertex,faceNormals,false);%returns a bool for collision 1=collision
end


%% IsIntersectionPointInsideTriangle
% Given a point which is known to be on the same plane as the triangle
% determine if the point is
% inside (result == 1) or
% outside a triangle (result ==0 )
function result = IsIntersectionPointInsideTriangle(intersectP,triangleVerts)

u = triangleVerts(2,:) - triangleVerts(1,:);
v = triangleVerts(3,:) - triangleVerts(1,:);

uu = dot(u,u);
uv = dot(u,v);
vv = dot(v,v);

w = intersectP - triangleVerts(1,:);
wu = dot(w,u);
wv = dot(w,v);

D = uv * uv - uu * vv;

% Get and test parametric coords (s and t)
s = (uv * wv - vv * wu) / D;
if (s < 0.0 || s > 1.0)        % intersectP is outside Triangle
    result = 0;
    return;
end

t = (uv * wu - uu * wv) / D;
if (t < 0.0 || (s + t) > 1.0)  % intersectP is outside Triangle
    result = 0;
    return;
end

result = 1;                      % intersectP is in Triangle
end

%% IsCollision 
% Given a robot model (robot), and trajectory (i.e. joint state vector) (qMatrix)
% and triangle obstacles in the environment (faces,vertex,faceNormals)
function result = IsCollision(robot,qMatrix,faces,vertex,faceNormals,returnOnceFound)
if nargin < 6
    returnOnceFound = true;
end
result = false;

for qIndex = 1:size(qMatrix,1)
    q = qMatrix(qIndex,:);
    % Get the transform of every joint (i.e. start and end of every link)
    tr = zeros(4,4,robot.n+1);
    tr(:,:,1) = robot.base;
    L = robot.links;
    for i = 1 : robot.n
        tr(:,:,i+1) = tr(:,:,i) * trotz(q(i)+L(i).offset) * transl(0,0,L(i).d) * transl(L(i).a,0,0) * trotx(L(i).alpha);
    end
    
    % Go through each link and also each triangle face
    % WHEN q = [0,0,0];
    for i = 1 : size(tr,3)-1  % reason tr,3 because the that shows the layers/amount of links
        for faceIndex = 1:size(faces,1) %faces is passed from Rectangular prism!!!
            vertOnPlane = vertex(faces(faceIndex,1)',:); %retrieve xyz from vetex1-8 depending on which faceIndex (in the for loop) eg 5 loop is face row 5.,
            %1,2,3 is the point correlating in the face matrix which points to vertex 1-8 to get the XYZ
            [intersectP,check] = LinePlaneIntersection(faceNormals(faceIndex,:),vertOnPlane,tr(1:3,4,i)',tr(1:3,4,i+1)');
            if check == 1 && IsIntersectionPointInsideTriangle(intersectP,vertex(faces(faceIndex,:)',:)) % returns 1 if intersects but we still need it know if its inside the triangles
                plot3(intersectP(1),intersectP(2),intersectP(3),'b*'); % if it is plot where it has intersected
                result = 1;
            end
            
        end
    end
    
end
end

