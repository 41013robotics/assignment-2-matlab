// Ultrasonic - Library for HR-SC04 Ultrasonic Ranging Module.
// Rev.4 (06/2012)
// J.Rodrigo ( www.jrodrigo.net )
// more info at www.ardublog.com

#include <Ultrasonic.h>

Ultrasonic ultrasonic(9,8); // (Trig PIN,Echo PIN)
String recv_str; 

void setup() {
  Serial.begin(115200); 
  Serial.setTimeout(20);
}

void loop() {
  recv_str = Serial.readString();

  if (recv_str.indexOf("ultrasonic") > -1) {
    Serial.print(ultrasonic.Ranging(CM)); // CM or INC
  }
}
